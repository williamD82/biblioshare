import React, { useState, useEffect } from "react";
import Theme from "../../styles/theme";
import { getBooksAvaiableApi } from "../../commons/api/routes/BooksAvaiable";
import { styled } from "styled-components";
import CardBook from "../CardBook";
import { ThreeDots } from "react-loader-spinner";
import { useNavigate } from "react-router-dom";
const Contenair = styled.div`
  background-color: ${Theme?.colors?.white};
  padding: ${Theme?.padding?.regular}px;
  margin-bottom: 20px;
  @media (max-width: 768px) {
  }
`;
const TextTitleBloc = styled.div`
  text-align: center;
  max-width: 100%;
  max-height: 100%;
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
`;
const ContenairAllBooks = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  @media (max-width: 768px) {
  }
`;
const Contenairbtn = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-top: 60px;
  @media (max-width: 768px) {
  }
`;
const BtnSeeAllBooks = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;
const BooksAvailable = () => {
  const [books, setBooks] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    getBooksAvaiableApi()
      .then((data) => {
        setBooks(data);
        // console.log(data?.records);
      })
      .catch((error) => {
        console.log("erreur", error);
      });
  }, []);
  let arraybooksRandom = [];
  if (books?.records) {
    // Si books.records est défini, je le stocke dans booksRandom
    const booksRandom = books.records;
    // Je trie booksRandom de manière aléatoire avec la méthode .sort()
    booksRandom.sort(function () {
      return Math.random() - 0.5;
    });
    // Je recuperer les cinq premiers éléments triés et les stockent dans arraybooksRandom
    arraybooksRandom = booksRandom.slice(0, 10);
  }
  return (
    <Contenair>
      <TextTitleBloc>Sélection de livres disponibles</TextTitleBloc>
      <ContenairAllBooks>
        {books ? (
          arraybooksRandom?.map((b, index) => (
            <CardBook
              key={index}
              name={b?.fields?.name}
              image={b?.fields?.image?.[0]?.url}
              author={b?.fields?.author}
            />
          ))
        ) : (
          <ThreeDots
            visible={true}
            height="100"
            width="120"
            color={`${Theme?.colors?.primary?.default}`}
            radius="9"
            ariaLabel="three-dots-loading"
            wrapperStyle={{}}
            wrapperClass=""
          />
        )}
      </ContenairAllBooks>
      <Contenairbtn>
        <BtnSeeAllBooks onClick={() => navigate(`/all-books`)}>
          Voir plus de livres
        </BtnSeeAllBooks>
      </Contenairbtn>
    </Contenair>
  );
};

export default BooksAvailable;
