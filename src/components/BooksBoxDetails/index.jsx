import React, { useState, useEffect } from "react";
import { ThreeDots } from "react-loader-spinner";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import { getBooksBoxDetailsApi } from "../../commons/api/routes/BooksBox";
import { Theme } from "../../styles/theme";
export const BooksBoxDetails = () => {
  const [booksBoxDetailsId, setBooksBoxDetailsId] = useState();
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    getBooksBoxDetailsApi(id)
      .then((data) => {
        setBooksBoxDetailsId(data);
        console.log("data", data);
      })
      .catch((error) => {
        console.log("erreur", error);
      });
  }, [id]);

  // Je crée une fonction pour trouver les livres disponible.
  const getBooksAvaiable = (booksDeposed, booksRemoved) => {
    const resultBooksFound = [];
    for (let i = 0; i < booksDeposed.length; i++) {
      let booksFound = false;
      for (let j = 0; j < booksRemoved.length; j++) {
        if (booksDeposed[i] === booksRemoved[j]) {
          booksFound = true;
          break;
        }
      }
      if (!booksFound) {
        resultBooksFound.push(booksDeposed[i]);
      }
    }
    return resultBooksFound;
  };

  const booksDeposed =
    booksBoxDetailsId?.fields?.["name (from booksDeposed)"] ?? [];
  const booksRemoved =
    booksBoxDetailsId?.fields?.["name (from booksRemoved)"] ?? [];

  const booksDispo = getBooksAvaiable(booksDeposed, booksRemoved);
  console.log("booksDispo", booksDispo);

  return (
    <div
      style={{
        marginTop: "120px",
        display: "flex",
        flexWrap: "wrap",
        justifyContent: "space-evenly",
      }}
    >
      <div>
        <h3>Livres déposés</h3>
        <div>
          {booksBoxDetailsId?.fields?.["name (from booksDeposed)"]?.map(
            (n, index) => (
              <li key={index}>{n}</li>
            )
          )}
        </div>
      </div>
      <div>
        <h3>Livres non disponible</h3>
        <div>
          {booksBoxDetailsId?.fields?.["name (from booksRemoved)"]?.map(
            (n, index) => (
              <li key={index}>{n}</li>
            )
          )}
        </div>
      </div>
      <div>
        <h3>Livres disponible</h3>
        <div>
          {booksBoxDetailsId ? (
            booksDispo?.map((n, index) => <li key={index}>{n}</li>)
          ) : (
            <ThreeDots
              visible={true}
              height="100"
              width="120"
              color={`${Theme?.colors?.primary?.default}`}
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClass=""
            />
          )}
        </div>
      </div>
      <button
        style={{ height: "30px" }}
        onClick={() => navigate(`/all-books-box`)}
      >
        Retour aux boites à livres
      </button>
    </div>
  );
};

export default BooksBoxDetails;
