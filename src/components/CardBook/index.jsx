import React from "react";
import { styled } from "styled-components";
import Theme from "../../styles/theme";
import pasdelivre from "../../assets/images/pasdecouverturedelivre.png";

const Contenair = styled.div`
  margin-right: 40px;
  height: 250px;
  width: 190px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: space-evenly;
  margin-top: ${Theme?.margin?.regular * 3}px;
  background: white;
  border-radius: 5px;
`;

const ImgBook = styled.img`
  min-height: 100%;
  min-width: 190px;
  border-radius: 5px;
  &:hover {
    box-shadow: 0 0 10px 5px ${Theme?.colors?.primary?.default};
    cursor: pointer;
    min-height: 105%;
    min-width: 200px;
  }
`;

const ImgNoBook = styled.img`
  height: 150px;
  width: auto;
  border-radius: 5px;
`;

const ContenairName = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 5px;
`;

const ContenairAuthor = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
`;

const CardBook = ({ image, name, author }) => {
  return (
    <Contenair>
      {image ? (
        <ImgBook src={image} alt="" />
      ) : (
        <ImgNoBook src={pasdelivre} alt="" />
      )}
      <ContenairName>{name}</ContenairName>
      <ContenairAuthor>{author}</ContenairAuthor>
    </Contenair>
  );
};

export default CardBook;
