import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Description from "../../components/Description";
import Header from "../../components/Header";
import { styled } from "styled-components";
import BooksAvailable from "../../components/BooksAvailable";
import DescriptionTwo from "../../components/DescriptionTwo";
import Footer from "../../components/Footer";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const HomeRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <Description />
        <BooksAvailable />
        <DescriptionTwo />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default HomeRoute;
