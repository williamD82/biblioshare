import { APP_NAME } from "../../commons/constants/index";
import baldescription from "../../assets/images/bal_Description.png";
import Theme from "../../styles/theme";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../../components/AuthProvider";
import { useContext } from "react";
import { styled } from "styled-components";
const Contenair = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: ${Theme?.margin?.regular * 4}px;
  margin-bottom: ${Theme?.margin?.regular}px;
  background: ${Theme?.colors?.background?.default};
  @media (max-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: space-around;
    margin-top: ${Theme?.margin?.regular * 4}px;
    margin-bottom: ${Theme?.margin?.regular}px;
    background: ${Theme?.colors?.background?.default};
    flex-wrap: wrap;
  }
`;
const ContenairTextBtn = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: ${Theme?.padding?.regular}px;
  @media (max-width: 768px) {
    margin-top: ${Theme?.margin?.regular * 2}px;
  }
`;
const TextDescriptionImportant = styled.div`
  max-width: 90%;
  max-height: 100%;
  margin-bottom: ${Theme?.margin?.regular}px;
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
`;
const TextDescription = styled.div`
  max-width: 70%;
  max-height: 100%;
  font-size: ${Theme?.fontSize?.regular}px;
`;
const ImageDescription = styled.img`
  max-width: 50%;
  max-height: 100%;
  border-radius: 40% 0% 20% 90%;
  @media (max-width: 768px) {
    max-width: 100%;
    max-height: 100%;
    border-radius: 0% 40% 90% 20%;
  }
`;
const ButtonSignin = styled.button`
  min-height: 55px;
  min-width: 310px;
  font-size: ${Theme?.fontSize?.regular}px;
  padding: ${Theme?.padding?.regular}px;
  border-radius: 5px;
  margin-top: 50px;
  cursor: pointer;
  border: 2px solid ${Theme?.colors?.primary?.default};
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;
export const Description = () => {
  const navigate = useNavigate();
  const { isLoggedIn } = useContext(AuthContext);

  return (
    <Contenair>
      <ContenairTextBtn>
        <TextDescriptionImportant>
          {APP_NAME} est une application innovante qui révolutionne la manière
          dont vous découvrez, partagez et échangez des livres.
        </TextDescriptionImportant>{" "}
        <TextDescription>
          Avec {APP_NAME}, plongez dans un univers littéraire communautaire où
          la lecture devient une expérience collaborative et enrichissante.
        </TextDescription>
        {isLoggedIn ? (
          <ButtonSignin onClick={() => navigate(`/dashboard`)}>
            Mon dashboard {APP_NAME} !
          </ButtonSignin>
        ) : (
          <ButtonSignin onClick={() => navigate(`/register`)}>
            Commencer à utiliser {APP_NAME} !
          </ButtonSignin>
        )}
      </ContenairTextBtn>
      <ImageDescription src={baldescription} alt="" />
    </Contenair>
  );
};

export default Description;
