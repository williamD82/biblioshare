import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import LoginForm from "../../forms/Login";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const LoginFormRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <LoginForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default LoginFormRoute;
