import Theme from "../../styles/theme";
import { GiPositionMarker } from "react-icons/gi";
import { styled } from "styled-components";
const Contenair = styled.div`
  margin-right: 40px;
  width: 305px;
  border: 2px solid black;
  height: 400px;
  flex-direction: column;
  margin-top: 60px;
  border-radius: 5px;
  @media (max-width: 768px) {
  }
`;
const ContenairToto = styled.div`
  height: 300px;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: center;
  @media (max-width: 768px) {
  }
`;
const ImgBook = styled.img`
  min-height: 100%;
  min-width: 100%;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  @media (max-width: 768px) {
  }
`;
const ContenairCity = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-top: 5px;
  margin-bottom: 5px;
  @media (max-width: 768px) {
  }
`;
const ContenairAdress = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  display: flex;
  align-items: flex-end;
  @media (max-width: 768px) {
  }
`;
const ButtonSeeBooks = styled.button`
  min-height: 30px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-top: ${Theme?.margin?.regular / 2}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
`;
export const CardBooksBox = ({ image, city, adress, postalCode, onClick }) => {
  return (
    <Contenair>
      <ContenairToto>
        <ImgBook src={image} alt="" />
        <ContenairCity>
          {postalCode} {city}
        </ContenairCity>
        <ContenairAdress>
          <GiPositionMarker size={15} />
          {adress}
        </ContenairAdress>
        <ButtonSeeBooks onClick={onClick}>Voir les livres</ButtonSeeBooks>
      </ContenairToto>
    </Contenair>
  );
};

export default CardBooksBox;
