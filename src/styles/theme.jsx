const colors = {
  white: "#FFFFFF",
  red: "#F24750",
  green: "#1BC773",
  black: "black",
  primary: {
    default: "#987C92",
    dark: "#83677D",
  },
  secondary: {
    default: "#C4BDB5",
    dark: "#4A433B",
  },
  accent: {
    default: "#A5A48D",
    dark: "#72715A",
  },
  text: {
    default: "#141013",
    dark: "#EFEBEE",
  },
  error: {
    default: "#F24750",
    dark: "#F24750",
  },
  warning: {
    default: "#FF8C4C",
  },
  success: {
    default: "#1BC773",
  },
  disabled: {
    default: "#819CAD",
  },
  boxShadow: {
    default: "0 0 10px 5px #987C92",
  },
  background: {
    default: "#FAF9F9",
    dark: "#060505",
  },
};
const fontSize = {
  small: 12,
  regular: 14,
  xlarge: 20,
  h1: 67.36,
  h2: 50.56,
  h3: 37.92,
  h4: 28.48,
  h5: 21.28,
};
const padding = {
  regular: 20,
};
const margin = {
  regular: 20,
};
export const Theme = {
  colors,
  fontSize,
  padding,
  margin,
  innerSize: 1170,
  background: colors.white,
  text: colors.black,
  success: colors.green,
  danger: colors.red,
};
export default Theme;
