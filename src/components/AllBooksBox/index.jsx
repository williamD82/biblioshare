import React, { useState, useEffect } from "react";
import Theme from "../../styles/theme";
import { getBooksBoxApi } from "../../commons/api/routes/BooksBox";
import { ThreeDots } from "react-loader-spinner";
import CardBooksBox from "../CardBooksBox";
import { useNavigate } from "react-router";
import { styled } from "styled-components";

const Contenair = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: ${Theme?.margin?.regular * 4}px;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;

export const AllBooksBox = () => {
  const [booksBox, setBooksBox] = useState();
  const navigate = useNavigate();

  useEffect(() => {
    getBooksBoxApi()
      .then((data) => {
        setBooksBox(data);
        console.log(data?.records);
      })
      .catch((error) => {
        console.log("erreur", error);
      });
  }, []);
  return (
    <Contenair>
      {booksBox ? (
        booksBox?.records?.map((bb, index) => (
          <CardBooksBox
            key={index}
            image={bb?.fields?.image?.[0]?.url}
            postalCode={bb?.fields?.postalCode}
            city={bb?.fields?.city}
            adress={bb?.fields?.adress}
            onClick={() => navigate(`/all-books-box-details/${bb?.id}`)}
          />
        ))
      ) : (
        <ThreeDots
          visible={true}
          height="100"
          width="120"
          color={`${Theme?.colors?.primary?.default}`}
          radius="9"
          ariaLabel="three-dots-loading"
          wrapperStyle={{}}
          wrapperClass=""
        />
      )}
    </Contenair>
  );
};

export default AllBooksBox;
