import React, { useContext } from "react";
import { AuthContext } from "../../components/AuthProvider";
import CardBook from "../CardBook";
import Theme from "../../styles/theme";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";

const Contenair = styled.div`
  margin-top: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;
const ContenairSubTitleBooks = styled.div`
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;
const TextTitle = styled.div`
  text-align: center;
  max-width: 100%;
  max-height: 100%;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  font-size: ${Theme?.fontSize?.h2}px;
  font-weight: bold;
  @media (max-width: 768px) {
    margin-bottom: ${Theme?.margin?.regular}px;
    margin-top: ${Theme?.margin?.regular * 6}px;
  }
`;
const TextSubTitle = styled.div`
  font-size: ${Theme?.fontSize?.h5}px;
  font-weight: bold;
  color: ${Theme?.colors?.primary?.default};
  margin-left: ${Theme?.margin?.regular}px;
`;
const TextSubSubTitle = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  color: ${Theme?.colors?.secondary?.default};
  margin-left: ${Theme?.margin?.regular}px;
`;
const ImgDashboard = styled.img`
  height: 120px;
  margin-left: ${Theme?.margin?.regular}px;
  border: 2px solid ${Theme?.colors?.secondary?.default};
  border-radius: 15px;
`;
const TextSubTitleNO = styled.div`
  font-size: ${Theme?.fontSize?.h5}px;
  font-weight: bold;
  color: ${Theme?.colors?.secondary?.default};
  margin-left: ${Theme?.margin?.regular}px;
`;

const ContenairBooks = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-evenly;
  @media (max-width: 768px) {
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    align-content: center;
  }
`;
const ContenairProfil = styled.div`
  display: inline-flex;
  align-items: center;
  margin-top: ${Theme?.margin?.regular * 2}px;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  @media (max-width: 768px) {
  }
`;
const Divider = styled.div`
  height: 2px;
  margin-bottom: ${Theme?.margin?.regular * 4}px;
  margin-left: ${Theme?.margin?.regular * 4}px;
  margin-right: ${Theme?.margin?.regular * 4}px;
  background: ${Theme?.colors?.secondary?.default};
  @media (max-width: 768px) {
    height: 2px;
    margin-bottom: ${Theme?.margin?.regular * 4}px;
    background: ${Theme?.colors?.secondary?.default};
  }
`;
const BtnAddBooks = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
  @media (max-width: 768px) {
    margin-top: ${Theme?.margin?.regular * 4}px;
  }
`;
const BtnEditProfil = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-left: ${Theme?.margin?.regular * 2}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
  @media (max-width: 768px) {
    margin-top: ${Theme?.margin?.regular * 4}px;
  }
`;
const Dashboard = () => {
  const { userData } = useContext(AuthContext);
  const navigate = useNavigate();
  console.log(userData);

  return (
    <Contenair>
      <TextTitle>Dashboard</TextTitle>
      <TextSubTitle>Mon profil</TextSubTitle>
      <ContenairProfil>
        <TextSubSubTitle>{userData?.firstName}</TextSubSubTitle>
        <TextSubSubTitle>{userData?.lastName}</TextSubSubTitle>
        <ImgDashboard src={userData?.image[0]?.url} />
        <BtnEditProfil>Modifier mon profil</BtnEditProfil>
      </ContenairProfil>
      <Divider></Divider>
      <ContenairSubTitleBooks>
        <TextSubTitle>Livres déposés</TextSubTitle>
        <ContenairBooks>
          {userData ? (
            userData["name (from BooksDeposed )"]?.map((bD, index) => (
              <CardBook
                key={index}
                name={bD}
                image={userData["image (from BooksDeposed )"][index]?.url}
                author={userData["author (from booksDeposed)"][index]}
              />
            ))
          ) : (
            <TextSubTitleNO>Aucun livres déposés</TextSubTitleNO>
          )}
          <BtnAddBooks onClick={() => navigate(`/addBookDeposed`)}>
            Ajouter un livre déposé
          </BtnAddBooks>
        </ContenairBooks>
      </ContenairSubTitleBooks>
      <Divider></Divider>
      <ContenairSubTitleBooks>
        <TextSubTitle>Livres empruntés</TextSubTitle>
        <ContenairBooks>
          {userData ? (
            userData?.["name (from BookRemoved)"]?.map((bR, index) => (
              <CardBook
                key={index}
                name={bR}
                image={
                  userData["image (from image (from BooksDeposed )"][index]?.url
                }
                author={userData["author (from booksDeposed)"][index]}
              />
            ))
          ) : (
            <TextSubTitleNO>Aucun livres empruntés</TextSubTitleNO>
          )}
          <BtnAddBooks>Ajouter un livre emprunté</BtnAddBooks>
        </ContenairBooks>
      </ContenairSubTitleBooks>
    </Contenair>
  );
};

export default Dashboard;
