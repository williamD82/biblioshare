import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomeRoute from "./Home";
import AllBooksRoute from "./AllBooks";
import AllBooksBoxRoute from "./AllBooksBox";
import RegisterFormRoute from "./RegisterForm";
import LoginFormRoute from "./LoginForm";
import BooksBoxRoute from "./BooksBox";
import { AuthProvider } from "../components/AuthProvider";
import DashBoardRoute from "./DashBoard";
import AddBookDeposedRoute from "./AddBookDeposed";

export const Router = () => {
  return (
    <BrowserRouter basename="/">
      <AuthProvider>
        <Routes>
          <Route path="/" element={<HomeRoute />} />
          <Route path="/all-books" element={<AllBooksRoute />} />
          <Route path="/all-books-box" element={<AllBooksBoxRoute />} />
          <Route
            path="/all-books-box-details/:id"
            element={<BooksBoxRoute />}
          />
          <Route path="/register" element={<RegisterFormRoute />} />
          <Route path="/login" element={<LoginFormRoute />} />
          <Route path="/dashboard" element={<DashBoardRoute />} />
          <Route path="/addBookDeposed" element={<AddBookDeposedRoute />} />
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
};

export default Router;
