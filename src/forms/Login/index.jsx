import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../components/AuthProvider";
import Theme from "../../styles/theme";
import { authenticateUser } from "../../commons/api/routes/Users";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { styled } from "styled-components";

const Contenair = styled.div`
  display: flex;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 2}px;
`;

const ContenairForm = styled.form`
  height: 380px;
  width: 300px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 5}px;
  margin-bottom: ${Theme?.margin?.regular * 5}px;
  border-radius: 5px;
  flex-wrap: wrap;
  align-content: center;
  align-items: flex-start;
`;

const ContenairInput = styled.input`
  min-height: 20px;
  min-width: 90%;
  border: 2px solid ${Theme?.colors?.primary?.default};
  border-radius: 5px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;

const ButtonSignin = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-top: ${Theme?.margin?.regular}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;

const TextLabel = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  margin-right: ${Theme?.margin?.regular / 2}px;
`;

const TextSpan = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  color: ${Theme?.colors?.error?.default};
`;
const ContenairTextError = styled.div`
  display: flex;
  align-items: flex-end;
`;
const ContenairTextP = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;
export const LoginForm = () => {
  const { logIn } = useAuth();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    try {
      const { firstName, lastName, password } = data;
      const response = await authenticateUser(firstName, lastName, password);

      if (response.success) {
        console.log("success", "Vous êtes connecté");
        logIn(response.data);
        notifySucces(data);
        setTimeout(() => {
          navigate("/dashboard");
        }, 2500);
      } else {
        notifyError(response.message);
      }
    } catch (error) {
      console.error("Erreur lors de la connexion:", error);
      notifyError("Une erreur s'est produite lors de la connexion");
    }
  };

  const notifySucces = (data) => {
    toast.success(
      `
    Bienvenu ${data?.firstName}`,
      {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "colored",
      }
    );
  };
  const notifyError = () => {
    toast.error("Erreur lors de la connexion à votre compte", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  return (
    <Contenair>
      <ContenairForm onSubmit={handleSubmit(onSubmit)}>
        <ContenairTextP>Connectez-vous</ContenairTextP>
        <ContenairTextError>
          <TextLabel>Prénom</TextLabel>
          {errors?.firstName && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("firstName", { required: true })} />
        <ContenairTextError>
          <TextLabel>Nom</TextLabel>
          {errors?.lastName && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("lastName", { required: true })} />
        <ContenairTextError>
          <TextLabel>Mot de passe</TextLabel>
          {errors?.password && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("password", { required: true })} />
        <ButtonSignin type="submit">Se connecter</ButtonSignin>
        <ToastContainer />
      </ContenairForm>
    </Contenair>
  );
};

export default LoginForm;
