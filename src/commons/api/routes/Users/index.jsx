import axios from "axios";
import { BASE_URL, TOKEN } from "../../../constants/index";

export const authenticateUser = async (username, lastname, password) => {
  try {
    const response = await axios.get(`${BASE_URL}/Users`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
      params: {
        filterByFormula: `AND({firstName} = '${username}', {lastName} = '${lastname}',{password} = '${password}')`,
        maxRecords: 1,
      },
    });

    if (response.data.records.length > 0) {
      return { success: true, data: response.data.records[0].fields };
    } else {
      return {
        success: false,
        message: "Informations de connexion incorrectes",
      };
    }
  } catch (error) {
    console.error("Erreur lors de la connexion:", error);
    return {
      success: false,
      message: "Une erreur s'est produite lors de la connexion",
    };
  }
};

export const createUser = async (data) => {
  try {
    const response = await axios.post(
      `${BASE_URL}/Users`,
      {
        records: [
          {
            fields: {
              firstName: data?.firstName,
              lastName: data?.lastName,
              password: data?.password,
              email: data?.email,
            },
          },
        ],
      },
      {
        headers: {
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    );

    return response.data;
  } catch (error) {
    console.error("Erreur lors de la création de l'utilisateur:", error);
    return {
      success: false,
      message: "Une erreur s'est produite lors de la création de l'utilisateur",
    };
  }
};
