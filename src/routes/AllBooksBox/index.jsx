import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import AllBooksBox from "../../components/AllBooksBox";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const AllBooksBoxRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <AllBooksBox />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default AllBooksBoxRoute;
