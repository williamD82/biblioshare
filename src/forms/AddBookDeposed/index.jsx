import React from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
// import { useAuth } from "../../components/AuthProvider";
import Theme from "../../styles/theme";
// import { authenticateUser } from "../../commons/api/routes/Users";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { styled } from "styled-components";
import { createBookDeposed } from "../../commons/api/routes/BooksAvaiable";
const Contenair = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: ${Theme?.margin?.regular * 2}px;
  @media (max-width: 768px) {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 40px;
    flex-direction: column-reverse;
  }
`;

const ContenairForm = styled.form`
  width: 300px;
  border: 2px solid ${Theme?.colors?.primary?.default};
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: ${Theme?.margin?.regular * 5}px;
  margin-bottom: ${Theme?.margin?.regular * 5}px;
  border-radius: 5px;
  flex-wrap: wrap;
  align-content: center;
  align-items: flex-start;
`;

const ContenairInput = styled.input`
  min-height: 20px;
  min-width: 90%;
  border: 2px solid ${Theme?.colors?.primary?.default};
  border-radius: 5px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;

const ButtonSignin = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;
const ButtonReturn = styled.button`
  min-height: 40px;
  min-width: 120px;
  border-radius: 5px;
  cursor: pointer;
  border: none;
  margin-right: ${Theme?.margin?.regular * 2}px;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;

const TextLabel = styled.div`
  font-size: ${Theme?.fontSize?.regular}px;
  font-weight: bold;
  margin-right: ${Theme?.margin?.regular / 2}px;
`;

const TextSpan = styled.div`
  font-size: ${Theme?.fontSize?.small}px;
  font-weight: bold;
  color: ${Theme?.colors?.error?.default};
`;
const ContenairTextError = styled.div`
  display: flex;
  align-items: flex-end;
`;
const ContenairTextRegister = styled.div`
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  margin-top: ${Theme?.margin?.regular}px;
  margin-bottom: ${Theme?.margin?.regular}px;
`;
const AddBookDeposedForm = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    console.log("data Create Book", data);
    try {
      await createBookDeposed(data);
      console.log("Livre crée avec succès");
      notifySucces(data);
      setTimeout(() => {
        navigate("/dashboard");
      }, 2000);
    } catch (e) {
      notifyError();
      console.log("erreur");
    }
  };

  const notifySucces = () => {
    toast.success(`livre créer avec succés `, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  const notifyError = () => {
    toast.error("Erreur lors de la création du livre", {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "colored",
    });
  };
  return (
    <Contenair>
      <ButtonReturn onClick={() => navigate(`/dashboard`)} type="submit">
        Retour à mes livres
      </ButtonReturn>
      <ContenairForm onSubmit={handleSubmit(onSubmit)}>
        <ContenairTextRegister>Créez votre livre</ContenairTextRegister>
        <ContenairTextError>
          <TextLabel>Nom</TextLabel>
          {errors?.name && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("name", { required: true })} />
        <ContenairTextError>
          <TextLabel>Auteur</TextLabel>
          {errors?.author && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("author", { required: true })} />
        {/* <ContenairTextError>
          <TextLabel>Image</TextLabel>
          {errors?.image && <TextSpan>Ce champs est requis</TextSpan>}
        </ContenairTextError>
        <ContenairInput {...register("image", { required: true })} /> */}
        <ButtonSignin type="submit">Ajouter</ButtonSignin>
        <ToastContainer />
      </ContenairForm>
    </Contenair>
  );
};

export default AddBookDeposedForm;
