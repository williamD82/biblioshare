import { BASE_URL, TOKEN } from "../../../constants/index";
import axios from "axios";

// Fonction asynchrone Get pour afficher les livres disponibles
export const getBooksAvaiableApi = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/BooksDeposed`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    });
    console.log(response.data);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
};

// Créer un bookDeposed
export const createBookDeposed = async (data) => {
  try {
    const response = await axios.post(
      `${BASE_URL}/BooksDeposed`,
      {
        records: [
          {
            fields: {
              name: data?.name,
              author: data?.author,
              image: data?.image,
            },
          },
        ],
      },
      {
        headers: {
          Authorization: `Bearer ${TOKEN}`,
        },
      }
    );

    return response.data;
  } catch (error) {
    console.error("Erreur lors de la création du livre:", error);
    return {
      success: false,
      message: "Une erreur s'est produite lors de la création du livre",
    };
  }
};
