import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import BooksBoxDetails from "../../components/BooksBoxDetails";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const BooksBoxRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <BooksBoxDetails />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default BooksBoxRoute;
