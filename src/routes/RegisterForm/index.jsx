import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import RegisterForm from "../../forms/Register";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const RegisterFormRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <RegisterForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default RegisterFormRoute;
