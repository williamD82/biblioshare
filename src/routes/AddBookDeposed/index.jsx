import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import AddBookDeposedForm from "../../forms/AddBookDeposed";
const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const AddBookDeposedRoute = () => {
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <AddBookDeposedForm />
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default AddBookDeposedRoute;
