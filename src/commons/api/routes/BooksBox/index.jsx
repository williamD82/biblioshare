import { BASE_URL, TOKEN } from "../../../constants/index";
import axios from "axios";

// Fonction asynchrone Get pour afficher les boites à livres
export const getBooksBoxApi = async () => {
  try {
    const response = await axios.get(`${BASE_URL}/BooksBox`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    });
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
};

// Fonction asynchrone Get pour afficher les détails des boites à livres
export const getBooksBoxDetailsApi = async (id) => {
  try {
    const response = await axios.get(`${BASE_URL}/BooksBox/${id}`, {
      headers: {
        Authorization: `Bearer ${TOKEN}`,
      },
    });
    // console.log(response.data);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
};
