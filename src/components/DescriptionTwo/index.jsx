import { APP_NAME } from "../../commons/constants/index";
import balDescriptionTwo from "../../assets/images/bal_2.png";
import Theme from "../../styles/theme";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
const Contenair = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: ${Theme?.margin?.regular}px;
  background: ${Theme?.colors?.background?.default};
  @media (max-width: 768px) {
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: ${Theme?.margin?.regular}px;
    background: ${Theme?.colors?.background?.default};
    flex-wrap: wrap;
  }
`;
const ContenairText = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  padding: 60px;
  @media (max-width: 768px) {
  }
`;
const TextDescriptionImportant = styled.div`
  max-width: 100%;
  max-height: 100%;
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
`;
const TextDescription = styled.div`
  margin-top: ${Theme?.margin?.regular}px;
  // max-width: 60%;
  max-height: 100%;
  font-size: ${Theme?.fontSize?.regular}px;
`;
const ImageDescription = styled.img`
  max-width: 40%;
  max-height: 100%;
  border-radius: 0% 40% 90% 20%;
  @media (max-width: 768px) {
    max-width: 100%;
    max-height: 100%;
    border-radius: 0% 40% 90% 20%;
  }
`;
const ButtonSignin = styled.button`
  min-height: 55px;
  min-width: 310px;
  font-size: ${Theme?.fontSize?.regular}px;
  padding: ${Theme?.padding?.regular}px;
  border-radius: 5px;
  margin-top: 50px;
  cursor: pointer;
  border: 2px solid ${Theme?.colors?.primary?.default};
  background-color: ${Theme?.colors?.primary?.default};
  color: ${Theme?.colors?.white};
  &:hover {
    border: 2px solid ${Theme?.colors?.primary?.default};
    background-color: transparent;
    color: ${Theme?.colors?.primary?.default};
  }
`;
export const DescriptionTwo = () => {
  const navigate = useNavigate();

  return (
    <Contenair>
      <ImageDescription src={balDescriptionTwo} alt="" />
      <ContenairText>
        <TextDescriptionImportant>
          Trouvez votre prochaine boîte à livres sur {APP_NAME} !
        </TextDescriptionImportant>
        <TextDescription>
          Explorez une manière unique de découvrir des boîtes à livres autour de
          vous, et plongez dans un univers de partage et de découverte
          littéraire avec {APP_NAME}. Notre plateforme conviviale et intuitive
          vous permet de localiser facilement les boîtes à livres à proximité,
          où chaque livre déposé ou emprunté contribue à nourrir la passion de
          la lecture et à créer des liens au sein de notre communauté de
          lecteurs.
        </TextDescription>
        <ButtonSignin onClick={() => navigate(`/all-books-box`)}>
          Rechercher une boite à livres sur {APP_NAME} !
        </ButtonSignin>
      </ContenairText>
    </Contenair>
  );
};

export default DescriptionTwo;
