import React, { useState, useEffect } from "react";
import { getBooksAvaiableApi } from "../../commons/api/routes/BooksAvaiable";
import { Theme } from "../../styles/theme";
import { ThemeProvider } from "styled-components";
import Header from "../../components/Header";
import { styled } from "styled-components";
import Footer from "../../components/Footer";
import CardBook from "../../components/CardBook";
import { APP_NAME } from "../../commons/constants";
import { ThreeDots } from "react-loader-spinner";

const Contenair = styled.div`
  @media (max-width: 768px) {
  }
`;
const TextTitleBloc = styled.div`
  text-align: center;
  max-width: 100%;
  max-height: 100%;
  margin-top: 90px;
  font-size: ${Theme?.fontSize?.xlarge}px;
  font-weight: bold;
  @media (max-width: 768px) {
    margin-top: 110px;
  }
`;
const ContenairAllBooks = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  margin-bottom: 80px;
  @media (max-width: 768px) {
  }
`;
const AllBooksRoute = () => {
  const [allBooks, setAllBooks] = useState();

  useEffect(() => {
    getBooksAvaiableApi()
      .then((data) => {
        setAllBooks(data);
        console.log(data?.records);
      })
      .catch((error) => {
        console.log("erreur", error);
      });
  }, []);
  return (
    <Contenair>
      <ThemeProvider theme={Theme}>
        <Header />
        <TextTitleBloc>
          Tous les livres disponibles avec {APP_NAME}
        </TextTitleBloc>
        <ContenairAllBooks>
          {allBooks ? (
            allBooks?.records?.map((b, index) => (
              <CardBook
                key={index}
                name={b?.fields?.name}
                image={b?.fields?.image?.[0]?.url}
                author={b?.fields?.author}
              />
            ))
          ) : (
            <ThreeDots
              visible={true}
              height="100"
              width="120"
              color={`${Theme?.colors?.primary?.default}`}
              radius="9"
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClass=""
            />
          )}
        </ContenairAllBooks>
        <Footer />
      </ThemeProvider>
    </Contenair>
  );
};

export default AllBooksRoute;
